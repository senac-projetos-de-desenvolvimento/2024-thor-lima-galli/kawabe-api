package com.thorgalli.kawabe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.entities.section.Section;
import com.thorgalli.kawabe.entities.section.SectionMutationDTO;
import com.thorgalli.kawabe.entities.section.SectionPositionDTO;
import com.thorgalli.kawabe.repositories.SectionRepository;

@Service
public class SectionService {

    private final UploadService uploadService;
    private final SectionRepository sectionRepository;

    @Autowired
    public SectionService(UploadService uploadService, SectionRepository sectionRepository) {
        this.uploadService = uploadService;
        this.sectionRepository = sectionRepository;
    }

    public Section createSection(SectionMutationDTO data, MultipartFile file) {
        String imageURL = null;
        Section newSection = new Section();

        if (file != null) {
            imageURL = uploadService.uploadImage(file);
            newSection.setImageURL(imageURL);
        }

        newSection.setName(data.name());
        newSection.setContent(data.content());
        newSection.setSectionLayout(data.sectionLayout());

        return sectionRepository.save(newSection);
    }

    public Section updateSection(Section section, SectionMutationDTO data, MultipartFile file) {
        String imageURL = section.getImageURL();

        if (file != null) {
            imageURL = uploadService.uploadImage(file);
        }

        section.setName(data.name());
        section.setContent(data.content());
        section.setSectionLayout(data.sectionLayout());
        section.setImageURL(imageURL);

        return sectionRepository.save(section);
    }

    public void updateSectionPositions(List<SectionPositionDTO> positionUpdates) {
        for (SectionPositionDTO update : positionUpdates) {
            Section section = sectionRepository.findById(update.id())
                    .orElseThrow(() -> new RuntimeException("Section not found with id: " + update.id()));
            section.setPosition(update.position());
            sectionRepository.save(section);
        }
    }
}
