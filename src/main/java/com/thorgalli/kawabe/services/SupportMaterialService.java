package com.thorgalli.kawabe.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.common.Visibility;
import com.thorgalli.kawabe.entities.course.Course;
import com.thorgalli.kawabe.entities.course.CourseQueryDTO;
import com.thorgalli.kawabe.entities.support_material.SupportMaterial;
import com.thorgalli.kawabe.entities.support_material.SupportMaterialByCoursseDTO;
import com.thorgalli.kawabe.entities.support_material.SupportMaterialMutationDTO;
import com.thorgalli.kawabe.entities.support_material.SupportMaterialQueryDTO;
import com.thorgalli.kawabe.repositories.CourseRepository;
import com.thorgalli.kawabe.repositories.SupportMaterialRepository;

@Service
public class SupportMaterialService {
    private final UploadService uploadService;
    private final SupportMaterialRepository supportMaterialRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public SupportMaterialService(UploadService uploadService,
            SupportMaterialRepository supportMaterialRepository,
            CourseRepository courseRepository) {
        this.uploadService = uploadService;
        this.supportMaterialRepository = supportMaterialRepository;
        this.courseRepository = courseRepository;
    }

    public List<SupportMaterialByCoursseDTO> getMaterialsGroupedByCourse() {
        List<SupportMaterial> materials = supportMaterialRepository.findAll();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean isAuthenticated = authentication != null && authentication.isAuthenticated();
        boolean isAuthorized = (authentication.getAuthorities().stream()
                .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN")
                        || role.getAuthority().equals("ROLE_PROFESSOR") || role.getAuthority().equals("ROLE_USER")));

        // Agrupar os materiais por curso, filtrando pela visibilidade conforme a role
        Map<UUID, SupportMaterialByCoursseDTO> groupedByCourse = materials.stream()
                .filter(material -> {
                    Visibility visibility = material.getVisibility();
                    if (isAuthenticated && isAuthorized) {
                        return visibility == Visibility.PUBLIC || visibility == Visibility.AUTHENTICATED;
                    } else {
                        return visibility == Visibility.PUBLIC; // Unauthenticated users can only see public materials
                    }
                })
                .collect(Collectors.toMap(
                        material -> material.getCourse().getId(), // Chave: ID do curso
                        material -> new SupportMaterialByCoursseDTO(
                                new CourseQueryDTO(
                                        material.getCourse().getId(),
                                        material.getCourse().getNamePortuguese(),
                                        material.getCourse().getNameJapanese(),
                                        material.getCourse().getImageURL(),
                                        material.getCourse().getDescription()),
                                new ArrayList<>(List.of(
                                        new SupportMaterialQueryDTO(
                                                material.getId(),
                                                material.getTitle(),
                                                material.getCourse().getNamePortuguese(),
                                                material.getResourceURL(),
                                                material.getVisibility())))),
                        (existing, replacement) -> { // Resolver conflitos (agrupando os materiais existentes)
                            existing.supportMaterials().addAll(replacement.supportMaterials());
                            return existing;
                        }));

        // Converter o mapa para a lista desejada
        return new ArrayList<>(groupedByCourse.values());
    }

    public SupportMaterial createSupportMaterial(SupportMaterialMutationDTO data, MultipartFile file) {
        // Retrieve course by ID
        Course course = courseRepository.findById(data.courseID())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Course not found with id: " + data.courseID()));

        // Handle file upload
        String resourceURL = data.resourceURL();
        if (file != null) {
            resourceURL = uploadService.uploadPDF(file);
        }

        // Create and save new SupportMaterial
        SupportMaterial supportMaterial = new SupportMaterial(data.title(), course, resourceURL, data.visibility());
        return supportMaterialRepository.save(supportMaterial);
    }

    public SupportMaterial updateSupportMaterial(UUID supportMaterialId, SupportMaterialMutationDTO data,
            MultipartFile file) {
        SupportMaterial supportMaterial = supportMaterialRepository.findById(supportMaterialId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "SupportMaterial not found with id: " + supportMaterialId));

        // Retrieve course by ID
        Course course = courseRepository.findById(data.courseID())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Course not found with id: " + data.courseID()));

        // Handle file upload
        String resourceURL = supportMaterial.getResourceURL();
        if (file != null) {
            resourceURL = uploadService.uploadPDF(file);
        }

        // Update and save SupportMaterial
        supportMaterial.setTitle(data.title());
        supportMaterial.setCourse(course);
        supportMaterial.setResourceURL(resourceURL);
        supportMaterial.setVisibility(data.visibility());

        return supportMaterialRepository.save(supportMaterial);
    }
}
