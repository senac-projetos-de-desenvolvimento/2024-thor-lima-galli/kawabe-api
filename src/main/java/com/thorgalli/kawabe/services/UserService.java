package com.thorgalli.kawabe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.thorgalli.kawabe.entities.course.Course;
import com.thorgalli.kawabe.entities.course.CourseQueryDTO;
import com.thorgalli.kawabe.entities.user.User;
import com.thorgalli.kawabe.entities.user.UserPutDTO;
import com.thorgalli.kawabe.entities.user.UserQueryDTO;
import com.thorgalli.kawabe.repositories.UserRepository;

@Service
public class UserService {

  private final UserRepository userRepository;
  private final BCryptPasswordEncoder passwordEncoder;
  private final CourseService courseService;

  @Autowired
  public UserService(UserRepository userRepository, CourseService courseService) {
    this.userRepository = userRepository;
    this.courseService = courseService;
    this.passwordEncoder = new BCryptPasswordEncoder(); // Initialize the password encoder
  }

  // Método para criar um novo usuário
  public User createUser(User userData) {
    User newUser = new User();
    newUser.setName(userData.getName());
    newUser.setEmail(userData.getEmail());
    newUser.setPassword(passwordEncoder.encode(userData.getPassword())); // Encrypt password
    newUser.setRole(userData.getRole());

    return userRepository.save(newUser);
  }

  // Método para atualizar um usuário existente
  public User updateUser(User existingUser, UserPutDTO userDTO) {
    existingUser.setName(userDTO.name());
    existingUser.setRole(userDTO.role());
    if (userDTO.courseID() != null) {
      Course newCourse = courseService.getCourseByID(userDTO.courseID());
      existingUser.setCourse(newCourse);
    }

    return userRepository.save(existingUser);
  }

  public UserQueryDTO getUserByEmail(String email) {
    User user = (User) userRepository.findByEmail(email);
    if (user == null) {
      throw new RuntimeException("User not found with email: " + email);
    }

    if (user.getCourse() == null) {
      return new UserQueryDTO(
          user.getId(),
          user.getName(),
          user.getEmail(),
          null,
          user.getRole());
    } else {
      return new UserQueryDTO(
          user.getId(),
          user.getName(),
          user.getEmail(),
          new CourseQueryDTO(
              user.getCourse().getId(),
              user.getCourse().getNamePortuguese(),
              user.getCourse().getNameJapanese(),
              user.getCourse().getImageURL(),
              user.getCourse().getDescription()),
          user.getRole());
    }
  }

}
