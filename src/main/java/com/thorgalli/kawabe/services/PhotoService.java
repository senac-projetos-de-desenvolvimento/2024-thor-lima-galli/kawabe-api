package com.thorgalli.kawabe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.entities.photo.Photo;
import com.thorgalli.kawabe.entities.photo.PhotoDTO;

@Service
public class PhotoService {
  private final UploadService uploadService;

  @Autowired
  public PhotoService(UploadService uploadService) {
    this.uploadService = uploadService;
  }
  
  public Photo createPhoto(PhotoDTO data, MultipartFile file) {
    String imageURL = null;

    if (file != null) {
      imageURL = uploadService.uploadImage(file);
    }

    Photo newPhoto = new Photo();
    newPhoto.setDescription(data.description());
    newPhoto.setImageURL(imageURL);

    return newPhoto;
  }

  public Photo updatePhoto(Photo photo, PhotoDTO data, MultipartFile file) {
    String imageURL = photo.getImageURL();

    if (file != null) {
      imageURL = uploadService.uploadImage(file);
    }

    photo.setDescription(data.description());
    photo.setImageURL(imageURL);

    return photo;
  }
}
