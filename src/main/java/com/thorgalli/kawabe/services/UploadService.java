package com.thorgalli.kawabe.services;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadService {
  @Value("${app.upload.dir}")
  private String uploadDir;

  public String uploadImage(MultipartFile file) {
    try {
      String filename = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
      Path filePath = Paths.get(uploadDir, filename);

      Files.createDirectories(filePath.getParent());

      Files.copy(file.getInputStream(), filePath);

      return "/uploads/" + filename;
    } catch (Exception e) {
      throw new RuntimeException("Failed to store image file: " + e.getMessage(), e);
    }
  }

  public String uploadPDF(MultipartFile file) {
    try {
      // Check if the file is a PDF
      if (!file.getContentType().equals("application/pdf")) {
        throw new RuntimeException("File must be a PDF");
      }

      // Generate a unique filename
      String filename = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
      Path filePath = Paths.get(uploadDir, filename);

      // Ensure the directory exists
      Files.createDirectories(filePath.getParent());

      // Save the file to the directory
      Files.copy(file.getInputStream(), filePath);

      return "/uploads/" + filename;
    } catch (Exception e) {
      throw new RuntimeException("Failed to store PDF file: " + e.getMessage(), e);
    }
  }
}
