package com.thorgalli.kawabe.services;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.entities.course.Course;
import com.thorgalli.kawabe.entities.course.CourseMutationDTO;
import com.thorgalli.kawabe.repositories.CourseRepository;

@Service
public class CourseService {

  private final UploadService uploadService;
  private final CourseRepository courseRepository;

  @Autowired
  public CourseService(UploadService uploadService, CourseRepository courseRepository) {
    this.uploadService = uploadService;
    this.courseRepository = courseRepository;
  }

  public Course createCourse(CourseMutationDTO data, MultipartFile file) {
    String imageURL = null;

    if (file != null) {
      imageURL = uploadService.uploadImage(file);
    }

    Course newCourse = new Course();
    newCourse.setNamePortuguese(data.namePortuguese());
    newCourse.setNameJapanese(data.nameJapanese());
    newCourse.setDescription(data.description());
    newCourse.setImageURL(imageURL);

    return newCourse;
  }

  public Course updateCourse(Course course, CourseMutationDTO data, MultipartFile file) {
    String imageURL = course.getImageURL();

    if (file != null) {
      imageURL = uploadService.uploadImage(file);
    }

    course.setNamePortuguese(data.namePortuguese());
    course.setNameJapanese(data.nameJapanese());
    course.setDescription(data.description());
    course.setImageURL(imageURL);

    return course;
  }

  public Course getCourseByID (UUID id) {
    return courseRepository.findById(id).orElse(null);
  }
}
