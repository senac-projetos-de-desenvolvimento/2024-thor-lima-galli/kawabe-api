package com.thorgalli.kawabe.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.thorgalli.kawabe.entities.course.Course;
import com.thorgalli.kawabe.entities.video.VideoList;
import com.thorgalli.kawabe.entities.video.VideoLink;
import com.thorgalli.kawabe.entities.video.VideoListMutationDTO;
import com.thorgalli.kawabe.entities.video.VideoListQueryDTO;
import com.thorgalli.kawabe.entities.video.VideoLinkDTO;
import com.thorgalli.kawabe.entities.video.VideoLinkQueryDTO;
import com.thorgalli.kawabe.repositories.CourseRepository;
import com.thorgalli.kawabe.repositories.VideoLinkRepository;
import com.thorgalli.kawabe.repositories.VideoListRepository;

@Service
public class VideoListService {
    private final VideoListRepository videoListRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public VideoListService(VideoListRepository videoListRepository, CourseRepository courseRepository,
            VideoLinkRepository videoLinkRepository) {
        this.videoListRepository = videoListRepository;
        this.courseRepository = courseRepository;
    }

    public VideoList updateVideoList(UUID videoListId, VideoListMutationDTO data) {
        VideoList videoList = videoListRepository.findById(videoListId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "VideoList not found with id: " + videoListId));
    
        // Update videoList title and course
        videoList.setTitle(data.title());
        Course course = courseRepository.findById(data.courseID())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Course not found with id: " + data.courseID()));
        videoList.setCourse(course);
    
        // Prepare for updating videoLinks
        List<VideoLink> existingVideoLinks = videoList.getVideoLinks();
        existingVideoLinks.clear(); // Clear existing collection
    
        for (VideoLinkDTO updatedVideoLinkDTO : data.videoLinks()) {
            VideoLink existingVideoLink = videoList.getVideoLinks().stream()
                    .filter(videoLink -> videoLink.getId() != null && videoLink.getId().toString().equals(updatedVideoLinkDTO.id()))
                    .findFirst()
                    .orElse(null);
        
            if (existingVideoLink != null) {
                // Update existing videoLink
                existingVideoLink.setTitle(updatedVideoLinkDTO.title());
                existingVideoLink.setUrl(updatedVideoLinkDTO.url());
            } else {
                // Create and add new videoLink
                VideoLink newVideoLink = new VideoLink(updatedVideoLinkDTO.title(), updatedVideoLinkDTO.url(), videoList);
                videoList.getVideoLinks().add(newVideoLink); 
            }
        }
    
        return videoListRepository.save(videoList);
    }




    public VideoList createVideoList(VideoListMutationDTO data) {
        Course course = courseRepository.findById(data.courseID())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Course not found with id: " + data.courseID()));
        VideoList videoList = new VideoList(data.title(), course);
        List<VideoLink> videoLinks = data.videoLinks().stream()
                .map(dto -> new VideoLink(dto.title(), dto.url(), videoList))
                .toList();
        videoList.setVideoLinks(videoLinks);
        return videoListRepository.save(videoList);
    }

    public VideoListQueryDTO toDTO(VideoList videoList) {
        List<VideoLinkQueryDTO> videoLinks = videoList.getVideoLinks().stream()
                .map(videoLink -> new VideoLinkQueryDTO(videoLink.getId(), videoLink.getTitle(), videoLink.getUrl()))
                .toList();
        return new VideoListQueryDTO(videoList.getId(), videoList.getTitle(), videoList.getCourse().getNamePortuguese(),
                videoLinks);
    }

    public void deleteVideoList(UUID videoListId) {
        VideoList videoList = videoListRepository.findById(videoListId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "VideoList not found with id: " + videoListId));
        videoListRepository.delete(videoList);
    }
}
