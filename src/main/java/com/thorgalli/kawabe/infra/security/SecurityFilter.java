package com.thorgalli.kawabe.infra.security;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.thorgalli.kawabe.repositories.UserRepository;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class SecurityFilter extends OncePerRequestFilter {

  private final TokenService tokenService;
  private final UserRepository userRepository;

  @Autowired
  public SecurityFilter(TokenService tokenService, UserRepository userRepository) {
    this.tokenService = tokenService;
    this.userRepository = userRepository;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    // Bypass routes
    String[] bypassRoutes = { "/public/", "/uploads/" };

    String requestURI = request.getRequestURI();

    boolean bypass = false;
    for (String route : bypassRoutes) {
      if (requestURI.startsWith(route)) {
        bypass = true;
        break;
      }
    }

    // Always try to set the user in the SecurityContext (even for public routes)
    var token = this.recoverToken(request);
    if (token != null) {
      var subjectEmail = tokenService.validateToken(token);
      UserDetails user = userRepository.findByEmail(subjectEmail);

      if (user != null) {
        var authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
      }
    }

    // Continue with the filter chain
    filterChain.doFilter(request, response);
  }

  private String recoverToken(HttpServletRequest request) {
    var token = request.getHeader("Authorization");
    if (token == null || token.isEmpty() || !token.startsWith("Bearer "))
      return null;
    return token.substring(7, token.length());
  }

}
