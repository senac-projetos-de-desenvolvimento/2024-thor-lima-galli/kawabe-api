package com.thorgalli.kawabe.common;

import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class VisibilityAwareEntity {

  @Enumerated(EnumType.STRING)
  @Column(nullable = true)
  private Visibility visibility;

  public Visibility getVisibility() {
    if (visibility == null) {
      return Visibility.PUBLIC;
    }
    return visibility;
  }

  public void setVisibility(Visibility visibility) {
    this.visibility = visibility;
  }
}
