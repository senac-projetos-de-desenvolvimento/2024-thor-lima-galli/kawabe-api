package com.thorgalli.kawabe.common;

public enum Visibility {
  PUBLIC,
  AUTHENTICATED,
  HIDDEN
}
