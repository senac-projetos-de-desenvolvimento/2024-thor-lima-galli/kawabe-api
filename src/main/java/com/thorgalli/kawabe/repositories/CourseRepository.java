package com.thorgalli.kawabe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

import com.thorgalli.kawabe.entities.course.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, UUID> {
  
}
