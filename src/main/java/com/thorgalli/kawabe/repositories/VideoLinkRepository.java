package com.thorgalli.kawabe.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thorgalli.kawabe.entities.video.VideoLink;

@Repository
public interface VideoLinkRepository extends JpaRepository<VideoLink, UUID> {
}
