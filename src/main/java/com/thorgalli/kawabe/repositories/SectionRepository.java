package com.thorgalli.kawabe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.thorgalli.kawabe.entities.section.Section;
import java.util.UUID;
import java.util.Optional;

@Repository
public interface SectionRepository extends JpaRepository<Section, UUID> {
  Optional<Section> findByNameIgnoreCase(String name);
}
