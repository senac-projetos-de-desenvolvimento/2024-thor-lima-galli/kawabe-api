package com.thorgalli.kawabe.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import com.thorgalli.kawabe.entities.user.User;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
  UserDetails findByEmail(String email);
}
