package com.thorgalli.kawabe.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thorgalli.kawabe.entities.photo.Photo;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, UUID> {
  
}
