package com.thorgalli.kawabe.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thorgalli.kawabe.entities.video.VideoList;

@Repository
public interface VideoListRepository extends JpaRepository<VideoList, UUID> {
}
