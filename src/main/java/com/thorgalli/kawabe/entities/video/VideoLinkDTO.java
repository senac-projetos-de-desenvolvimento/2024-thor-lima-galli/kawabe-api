package com.thorgalli.kawabe.entities.video;

public record VideoLinkDTO(String id, String title, String url) {
}
