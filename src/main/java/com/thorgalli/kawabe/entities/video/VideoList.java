package com.thorgalli.kawabe.entities.video;

import java.util.List;
import java.util.UUID;

import com.thorgalli.kawabe.entities.course.Course;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "video_lists")
public class VideoList {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String title;

    @ManyToOne
    @JoinColumn(name = "course_id", nullable = false)
    private Course course;

    @OneToMany(mappedBy = "videoList", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<VideoLink> videoLinks;

    public VideoList() {}

    public VideoList(String title, Course course) {
      this.title = title;
      this.course = course;
  }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<VideoLink> getVideoLinks() {
        return videoLinks;
    }

    public void setVideoLinks(List<VideoLink> videoLinks) {
        this.videoLinks = videoLinks;
    }

    public void addVideoLink(VideoLink videoLink) {
        videoLinks.add(videoLink);
        videoLink.setVideoList(this);
    }

    public void removeVideoLink(VideoLink videoLink) {
        videoLinks.remove(videoLink);
        videoLink.setVideoList(null);
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

}
