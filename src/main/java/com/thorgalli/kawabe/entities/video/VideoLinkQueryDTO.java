package com.thorgalli.kawabe.entities.video;

import java.util.UUID;

public record VideoLinkQueryDTO(UUID id,String title, String url) {
}
