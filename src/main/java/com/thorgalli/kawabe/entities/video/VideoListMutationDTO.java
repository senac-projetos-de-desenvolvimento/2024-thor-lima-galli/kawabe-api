package com.thorgalli.kawabe.entities.video;

import java.util.List;
import java.util.UUID;

public record VideoListMutationDTO(String title, UUID courseID, List<VideoLinkDTO> videoLinks) {
}