package com.thorgalli.kawabe.entities.video;

import java.util.List;
import java.util.UUID;

public record VideoListQueryDTO(UUID id, String title, String courseName, List<VideoLinkQueryDTO> videoLinks) {
}