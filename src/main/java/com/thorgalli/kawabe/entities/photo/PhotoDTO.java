package com.thorgalli.kawabe.entities.photo;

public record PhotoDTO(String description) {
}