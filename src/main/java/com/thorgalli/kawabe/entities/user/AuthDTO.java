package com.thorgalli.kawabe.entities.user;

public record AuthDTO(String email, String password) {  

}
