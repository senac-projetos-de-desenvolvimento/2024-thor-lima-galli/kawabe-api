package com.thorgalli.kawabe.entities.user;

public record RegisterDTO(String email, String password, String name, UserRole role) {  

}
