package com.thorgalli.kawabe.entities.user;

import java.util.UUID;

import com.thorgalli.kawabe.entities.course.CourseQueryDTO;

public record UserQueryDTO(
    UUID id,
    String name,
    String email,
    CourseQueryDTO course,
    UserRole role
) {}
