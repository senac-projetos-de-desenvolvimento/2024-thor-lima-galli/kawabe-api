package com.thorgalli.kawabe.entities.user;

import java.util.UUID;


public record UserPutDTO(
    String name,
    UUID courseID,
    UserRole role
) {}
