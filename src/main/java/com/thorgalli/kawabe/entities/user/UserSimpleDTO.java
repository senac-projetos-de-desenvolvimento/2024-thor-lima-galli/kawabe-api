package com.thorgalli.kawabe.entities.user;

import com.thorgalli.kawabe.entities.course.Course;

public record UserSimpleDTO(String email, String name, UserRole role, Course course) {  

}
