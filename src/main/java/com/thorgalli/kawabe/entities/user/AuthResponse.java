package com.thorgalli.kawabe.entities.user;

public record AuthResponse(String token, UserRole role) {
  
}
