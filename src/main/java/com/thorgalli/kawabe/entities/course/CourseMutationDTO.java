package com.thorgalli.kawabe.entities.course;

public record CourseMutationDTO(String namePortuguese, String nameJapanese, String description) {
}