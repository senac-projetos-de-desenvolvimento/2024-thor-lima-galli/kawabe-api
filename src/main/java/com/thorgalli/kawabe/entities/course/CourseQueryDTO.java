package com.thorgalli.kawabe.entities.course;

import java.util.UUID;

public record CourseQueryDTO(UUID id, String namePortuguese, String nameJapanese, String imageURL, String description) {
}