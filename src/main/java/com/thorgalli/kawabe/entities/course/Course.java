package com.thorgalli.kawabe.entities.course;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import com.thorgalli.kawabe.entities.user.User;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "courses")
public class Course implements Serializable { // Implement Serializable

  private static final long serialVersionUID = 1L; // Recommended for Serializable classes

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  @Column(nullable = false)
  private String namePortuguese;

  @Column(nullable = false)
  private String nameJapanese;

  @Column(nullable = false, columnDefinition = "TEXT")
  private String description;

  @Column(nullable = true)
  private String imageURL;

  @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
  private List<User> users;

  public Course() {
  }

  public Course(String namePortuguese, String nameJapanese, String description, String imageURL) {
    this.namePortuguese = namePortuguese;
    this.nameJapanese = nameJapanese;
    this.description = description;
    this.imageURL = imageURL;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getNamePortuguese() {
    return namePortuguese;
  }

  public void setNamePortuguese(String name) {
    this.namePortuguese = name;
  }

  public String getNameJapanese() {
    return nameJapanese;
  }

  public void setNameJapanese(String nameJapanese) {
    this.nameJapanese = nameJapanese;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getImageURL() {
    return imageURL;
  }

  public void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((namePortuguese == null) ? 0 : namePortuguese.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Course other = (Course) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Course [id=" + id + ", name=" + namePortuguese + "]";
  }

}
