package com.thorgalli.kawabe.entities.support_material;

import java.util.UUID;

import com.thorgalli.kawabe.common.Visibility;

public record SupportMaterialQueryDTO(UUID id, String title, String courseName, String resourceURL,
    Visibility visibility) {
}