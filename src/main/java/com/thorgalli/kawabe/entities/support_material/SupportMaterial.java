package com.thorgalli.kawabe.entities.support_material;

import java.util.UUID;

import com.thorgalli.kawabe.common.Visibility;
import com.thorgalli.kawabe.common.VisibilityAwareEntity;
import com.thorgalli.kawabe.entities.course.Course;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "support_materials")
public class SupportMaterial extends VisibilityAwareEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String title;

    @ManyToOne
    @JoinColumn(name = "course_id", nullable = false)
    private Course course;

    @Column(nullable = true)
    private String resourceURL;

    public SupportMaterial() {
    }

    public SupportMaterial(String title, Course course, String resourceURL, Visibility visibility) {
        this.title = title;
        this.course = course;
        this.resourceURL = resourceURL;
        this.setVisibility(visibility);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getResourceURL() {
        return resourceURL;
    }

    public void setResourceURL(String resourceURL) {
        this.resourceURL = resourceURL;
    }

}
