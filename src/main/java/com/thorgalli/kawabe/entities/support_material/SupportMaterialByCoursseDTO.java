package com.thorgalli.kawabe.entities.support_material;

import java.util.List;

import com.thorgalli.kawabe.entities.course.CourseQueryDTO;

public record SupportMaterialByCoursseDTO(CourseQueryDTO course, List<SupportMaterialQueryDTO> supportMaterials) {

}
