package com.thorgalli.kawabe.entities.support_material;

import java.util.UUID;

import com.thorgalli.kawabe.common.Visibility;

public record SupportMaterialMutationDTO(String title, UUID courseID, String resourceURL, Visibility visibility) {
}