package com.thorgalli.kawabe.entities.section;

public enum SectionLayoutEnum {
  IMAGE_BEHIND_TEXT, IMAGE_TOP_BIG, IMAGE_TOP_SMALL, TEXT_ONLY
}