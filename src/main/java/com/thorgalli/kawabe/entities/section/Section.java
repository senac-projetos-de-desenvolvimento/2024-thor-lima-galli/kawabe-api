package com.thorgalli.kawabe.entities.section;

import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "sections")
public class Section {
  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false, columnDefinition = "TEXT")
  private String content;

  @Enumerated(EnumType.STRING)
  @Column(nullable = true)
  private SectionLayoutEnum sectionLayout;

  @Column(nullable = true)
  private String imageURL;

  @Column(nullable = true)
  private int position;

  public Section() {
  }

  public Section(String name, String content, SectionLayoutEnum sectionLayout, String imageURL, int position) {
    this.name = name;
    this.content = content;
    this.sectionLayout = sectionLayout;
    this.imageURL = imageURL;
    this.position = position;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public SectionLayoutEnum getSectionLayout() {
    return sectionLayout;
  }

  public void setSectionLayout(SectionLayoutEnum sectionLayout) {
    this.sectionLayout = sectionLayout;
  }

  public String getImageURL() {
    return imageURL;
  }

  public void setImageURL(String imageURL) {
    this.imageURL = imageURL;
  }

  @Override
  public String toString() {
    return "Section [id=" + id + ", name=" + name + ", content=" + content + ", sectionLayout=" + sectionLayout
        + ", imageURL=" + imageURL + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((content == null) ? 0 : content.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((imageURL == null) ? 0 : imageURL.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((sectionLayout == null) ? 0 : sectionLayout.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Section other = (Section) obj;
    if (content == null) {
      if (other.content != null)
        return false;
    } else if (!content.equals(other.content))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (imageURL == null) {
      if (other.imageURL != null)
        return false;
    } else if (!imageURL.equals(other.imageURL))
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return sectionLayout != other.sectionLayout;
  }
}
