package com.thorgalli.kawabe.entities.section;

public record SectionMutationDTO(String name, String content, SectionLayoutEnum sectionLayout) {
}
