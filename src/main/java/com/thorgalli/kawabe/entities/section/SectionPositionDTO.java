package com.thorgalli.kawabe.entities.section;

import java.util.UUID;

public record SectionPositionDTO(UUID id, int position) {
}
