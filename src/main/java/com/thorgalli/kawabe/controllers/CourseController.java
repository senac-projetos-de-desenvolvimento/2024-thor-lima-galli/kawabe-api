package com.thorgalli.kawabe.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.entities.course.Course;
import com.thorgalli.kawabe.entities.course.CourseMutationDTO;
import com.thorgalli.kawabe.entities.course.CourseQueryDTO;
import com.thorgalli.kawabe.repositories.CourseRepository;
import com.thorgalli.kawabe.services.CourseService;

@RestController
@RequestMapping("/courses")
public class CourseController {
  private final CourseRepository courseRepository;
  private final CourseService courseService;

  @Autowired
  public CourseController(CourseRepository courseRepository, CourseService courseService) {
    this.courseRepository = courseRepository;
    this.courseService = courseService;
  }

  @GetMapping()
  public List<CourseQueryDTO> getCourses() {
    List<Course> courses = courseRepository.findAll();
    List<CourseQueryDTO> courseQueryDTOs = new ArrayList<>();
    for (Course course : courses) {
      CourseQueryDTO courseQueryDTO = new CourseQueryDTO(course.getId(), course.getNamePortuguese(),
          course.getNameJapanese(), course.getImageURL(), course.getDescription());
      courseQueryDTOs.add(courseQueryDTO);
    }
    return courseQueryDTOs;
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @GetMapping("/{id}")
  public ResponseEntity<CourseQueryDTO> getCourseById(@PathVariable UUID id) {
    Optional<Course> courseOptional = courseRepository.findById(id);
    if (courseOptional.isPresent()) {
      return ResponseEntity.ok(new CourseQueryDTO(courseOptional.get().getId(),
          courseOptional.get().getNamePortuguese(), courseOptional.get().getNameJapanese(),
          courseOptional.get().getImageURL(), courseOptional.get().getDescription()));
    } else {
      return new ResponseEntity("Course not found with id: " + id, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping(consumes = { "multipart/form-data" })
  public ResponseEntity<CourseQueryDTO> createCourse(
      @RequestPart("course") CourseMutationDTO createCourseDTO,
      @RequestPart(value = "file", required = false) MultipartFile file) {

    Course course = courseService.createCourse(createCourseDTO, file);
    courseRepository.save(course);

    return ResponseEntity.ok(new CourseQueryDTO(course.getId(), course.getNamePortuguese(), course.getNameJapanese(),
        course.getImageURL(), course.getDescription()));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteCourse(@PathVariable UUID id) {
    if (!courseRepository.existsById(id)) {
      return ResponseEntity.notFound().build();
    }

    courseRepository.deleteById(id);
    Map<String, String> response = new HashMap<>();
    response.put("message", "Curso com ID " + id + " foi excluído com sucesso.");
    return ResponseEntity.ok(response);
  }

  @PutMapping(path = "/{id}", consumes = { "multipart/form-data" })
  public ResponseEntity<CourseQueryDTO> updateCourse(@PathVariable UUID id,
      @RequestPart("course") CourseMutationDTO updateCourseDTO,
      @RequestPart(value = "file", required = false) MultipartFile file) {
    Course existingCourse = courseRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Course not found with id: " + id));

    Course updatedCourse = courseService.updateCourse(existingCourse, updateCourseDTO, file);
    courseRepository.save(updatedCourse);

    return ResponseEntity.ok(new CourseQueryDTO(updatedCourse.getId(), updatedCourse.getNamePortuguese(),
        updatedCourse.getNameJapanese(), updatedCourse.getImageURL(), updatedCourse.getDescription()));
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
      super(message);
    }
  }
}
