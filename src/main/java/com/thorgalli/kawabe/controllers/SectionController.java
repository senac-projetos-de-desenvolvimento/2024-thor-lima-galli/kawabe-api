package com.thorgalli.kawabe.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.entities.section.Section;
import com.thorgalli.kawabe.entities.section.SectionMutationDTO;
import com.thorgalli.kawabe.entities.section.SectionPositionDTO;
import com.thorgalli.kawabe.repositories.SectionRepository;
import com.thorgalli.kawabe.services.SectionService;

@RestController
@RequestMapping("/sections")
public class SectionController {
    private final SectionRepository sectionRepository;
    private final SectionService sectionService;

    @Autowired
    public SectionController(SectionRepository sectionRepository, SectionService sectionService) {
        this.sectionRepository = sectionRepository;
        this.sectionService = sectionService;
    }

    @GetMapping()
    public List<Section> getSections() {
        return sectionRepository.findAll(Sort.by(Sort.Order.asc("position").nullsLast()));
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @GetMapping("/{id}")
    public ResponseEntity<Section> getSectionById(@PathVariable UUID id) {
        Optional<Section> sectionOptional = sectionRepository.findById(id);
        if (sectionOptional.isPresent()) {
            return ResponseEntity.ok(sectionOptional.get());
        } else {
            return new ResponseEntity("Section not found with id: " + id, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(consumes = { "multipart/form-data" })
    public ResponseEntity<Section> createSection(
            @RequestPart("section") SectionMutationDTO createSectionDTO,
            @RequestPart(value = "file", required = false) MultipartFile file) {

        if (file == null) {
            Section section = sectionService.createSection(createSectionDTO, null);
            return ResponseEntity.ok(section);
        }
        Section section = sectionService.createSection(createSectionDTO, file);

        return ResponseEntity.ok(section);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSection(@PathVariable UUID id) {
        if (!sectionRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        sectionRepository.deleteById(id);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Section with ID " + id + " was successfully deleted.");
        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/{id}", consumes = { "multipart/form-data" })
    public ResponseEntity<Section> updateSection(@PathVariable UUID id,
            @RequestPart("section") SectionMutationDTO updateSectionDTO,
            @RequestPart(value = "file", required = false) MultipartFile file) {
        Section existingSection = sectionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Section not found with id: " + id));

        Section updatedSection = sectionService.updateSection(existingSection, updateSectionDTO, file);

        return ResponseEntity.ok(updatedSection);
    }

    @PutMapping("/positions")
    public ResponseEntity<?> updateSectionPositions(@RequestBody List<SectionPositionDTO> positionUpdates) {
        System.out.println("positionUpdates: " + positionUpdates);
        sectionService.updateSectionPositions(positionUpdates);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Section positions updated successfully.");
        return ResponseEntity.ok(response);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class ResourceNotFoundException extends RuntimeException {
        public ResourceNotFoundException(String message) {
            super(message);
        }
    }
}
