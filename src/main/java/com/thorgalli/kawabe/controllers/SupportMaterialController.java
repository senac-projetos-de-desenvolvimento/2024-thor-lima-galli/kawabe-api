package com.thorgalli.kawabe.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.entities.support_material.SupportMaterial;
import com.thorgalli.kawabe.entities.support_material.SupportMaterialMutationDTO;
import com.thorgalli.kawabe.entities.support_material.SupportMaterialQueryDTO;
import com.thorgalli.kawabe.repositories.SupportMaterialRepository;
import com.thorgalli.kawabe.services.SupportMaterialService;

@RestController
@RequestMapping("/support-materials")
public class SupportMaterialController {
  private final SupportMaterialService supportMaterialService;
  private final SupportMaterialRepository supportMaterialRepository;

  @Autowired
  public SupportMaterialController(SupportMaterialService supportMaterialService,
      SupportMaterialRepository supportMaterialRepository) {
    this.supportMaterialService = supportMaterialService;
    this.supportMaterialRepository = supportMaterialRepository;
  }

  @GetMapping
  public List<SupportMaterialQueryDTO> getSupportMaterials() {
    List<SupportMaterial> materials = supportMaterialRepository.findAll();
    List<SupportMaterialQueryDTO> supportMaterialQueryDTOs = new ArrayList<>();
    for (SupportMaterial material : materials) {
      SupportMaterialQueryDTO supportMaterialQueryDTO = new SupportMaterialQueryDTO(material.getId(),
          material.getTitle(), material.getCourse() != null ? material.getCourse().getNamePortuguese() : null,
          material.getResourceURL(), material.getVisibility());
      supportMaterialQueryDTOs.add(supportMaterialQueryDTO);
    }
    return supportMaterialQueryDTOs;

  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @GetMapping("/{id}")
  public ResponseEntity<SupportMaterial> getSupportMaterialById(@PathVariable UUID id) {
    Optional<SupportMaterial> supportMaterialOptional = supportMaterialRepository.findById(id);
    if (supportMaterialOptional.isPresent()) {
      return ResponseEntity.ok(supportMaterialOptional.get());
    } else {
      return new ResponseEntity("Support Material not found with id: " + id, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping(consumes = { "multipart/form-data" })
  public ResponseEntity<SupportMaterial> createSupportMaterial(
      @RequestPart("material") SupportMaterialMutationDTO createSupportMaterialDTO,
      @RequestPart(value = "file", required = false) MultipartFile file) {
    System.out.println("createSupportMaterialDTO: " + createSupportMaterialDTO);

    SupportMaterial newSupportMaterial = supportMaterialService.createSupportMaterial(createSupportMaterialDTO, file);
    supportMaterialRepository.save(newSupportMaterial);
    return ResponseEntity.ok(newSupportMaterial);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteSupportMaterial(@PathVariable UUID id) {
    if (!supportMaterialRepository.existsById(id)) {
      return ResponseEntity.notFound().build();
    }

    supportMaterialRepository.deleteById(id);
    Map<String, String> response = new HashMap<>();
    response.put("message", "Support Material with ID " + id + " was successfully deleted.");
    return ResponseEntity.ok(response);
  }

  @PutMapping("/{id}")
  public ResponseEntity<SupportMaterial> updateSupportMaterial(
      @PathVariable UUID id,
      @RequestPart("material") SupportMaterialMutationDTO updateSupportMaterialDTO,
      @RequestPart(value = "file", required = false) MultipartFile file) {

    SupportMaterial supportMaterial = supportMaterialService.updateSupportMaterial(id, updateSupportMaterialDTO, file);
    supportMaterialRepository.save(supportMaterial);
    return ResponseEntity.ok(supportMaterial);
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
      super(message);
    }
  }
}
