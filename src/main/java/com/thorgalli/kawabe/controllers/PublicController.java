package com.thorgalli.kawabe.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.thorgalli.kawabe.entities.course.Course;
import com.thorgalli.kawabe.entities.course.CourseQueryDTO;
import com.thorgalli.kawabe.entities.photo.Photo;
import com.thorgalli.kawabe.entities.section.Section;
import com.thorgalli.kawabe.entities.video.VideoList;
import com.thorgalli.kawabe.entities.video.VideoListQueryDTO;
import com.thorgalli.kawabe.entities.support_material.SupportMaterialByCoursseDTO;
import com.thorgalli.kawabe.repositories.CourseRepository;
import com.thorgalli.kawabe.repositories.PhotoRepository;
import com.thorgalli.kawabe.repositories.SectionRepository;
import com.thorgalli.kawabe.repositories.SupportMaterialRepository;
import com.thorgalli.kawabe.repositories.VideoListRepository;
import com.thorgalli.kawabe.services.SupportMaterialService;
import com.thorgalli.kawabe.services.VideoListService;

@RestController
@RequestMapping("/public")
public class PublicController {
  private final CourseRepository courseRepository;
  private final SectionRepository sectionRepository;
  private final PhotoRepository photoRepository;
  private final VideoListRepository videoListRepository;
  private final SupportMaterialRepository supportMaterialRepository;
  private final SupportMaterialService supportMaterialService;
  private final VideoListService videoListService;

  @Autowired
  public PublicController(
      CourseRepository courseRepository,
      SectionRepository sectionRepository,
      PhotoRepository photoRepository,
      VideoListRepository videoListRepository,
      SupportMaterialRepository supportMaterialRepository,
      SupportMaterialService supportMaterialService,
      VideoListService videoListService) {
    this.courseRepository = courseRepository;
    this.sectionRepository = sectionRepository;
    this.photoRepository = photoRepository;
    this.videoListRepository = videoListRepository;
    this.supportMaterialRepository = supportMaterialRepository;
    this.supportMaterialService = supportMaterialService;
    this.videoListService = videoListService;
  }

  @GetMapping("/courses")
  public List<CourseQueryDTO> getCourses() {
    List<Course> courses = courseRepository.findAll();
    List<CourseQueryDTO> courseQueryDTOs = new ArrayList<>();
    for (Course course : courses) {
      CourseQueryDTO courseQueryDTO = new CourseQueryDTO(course.getId(), course.getNamePortuguese(),
          course.getNameJapanese(), course.getImageURL(), course.getDescription());
      courseQueryDTOs.add(courseQueryDTO);
    }
    return courseQueryDTOs;
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @GetMapping("/courses/{id}")
  public ResponseEntity<Course> getCourseById(@PathVariable UUID id) {
    Optional<Course> courseOptional = courseRepository.findById(id);
    if (courseOptional.isPresent()) {
      return ResponseEntity.ok(courseOptional.get());
    } else {
      return new ResponseEntity("Course not found with id: " + id, HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/sections-other")
  public List<Section> getSections() {
    // Retrieve all sections from the repository
    List<Section> sections = sectionRepository.findAll();

    // Filter out sections with names "header" and "footer"
    List<Section> filteredSections = new ArrayList<>();
    for (Section section : sections) {
      if (!section.getName().equalsIgnoreCase("header") && !section.getName().equalsIgnoreCase("footer")) {
        filteredSections.add(section);
      }
    }

    // Return the filtered list of sections
    return filteredSections;
  }

  @GetMapping("/photos")
  public List<Photo> getPhotos() {
    return photoRepository.findAll();
  }

  @GetMapping("/video-lists")
  public ResponseEntity<List<VideoListQueryDTO>> getVideos() {
    List<VideoList> videoLists = videoListRepository.findAll();
    List<VideoListQueryDTO> videoListQueryDTOs = new ArrayList<>();
    for (VideoList videoList : videoLists) {
      VideoListQueryDTO videoListQueryDTO = videoListService.toDTO(videoList);
      videoListQueryDTOs.add(videoListQueryDTO);
    }
    return ResponseEntity.ok(videoListQueryDTOs);
  }

  @GetMapping("/header")
  public ResponseEntity<Section> getSectionByName() {
    Optional<Section> sectionOptional = sectionRepository.findByNameIgnoreCase("header");
    if (sectionOptional.isPresent()) {
      return ResponseEntity.ok(sectionOptional.get());
    } else {
      return new ResponseEntity("Section not found with header", HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/footer")
  public ResponseEntity<Section> getFooterSection() {
    Optional<Section> sectionOptional = sectionRepository.findByNameIgnoreCase("footer");
    if (sectionOptional.isPresent()) {
      return ResponseEntity.ok(sectionOptional.get());
    } else {
      return new ResponseEntity("Section not found with footer", HttpStatus.NOT_FOUND);
    }
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @GetMapping("/sections/{id}")
  public ResponseEntity<Section> getSectionById(@PathVariable UUID id) {
    Optional<Section> sectionOptional = sectionRepository.findById(id);
    if (sectionOptional.isPresent()) {
      return ResponseEntity.ok(sectionOptional.get());
    } else {
      return new ResponseEntity("Section not found with id: " + id, HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/support-materials")
  public List<SupportMaterialByCoursseDTO> getSupportMaterials() {
    try {
      return supportMaterialService.getMaterialsGroupedByCourse();
    } catch (Exception e) {
      throw new ResourceNotFoundException("Support materials not found");
    }
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
      super(message);
    }
  }
}
