package com.thorgalli.kawabe.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thorgalli.kawabe.entities.video.VideoList;
import com.thorgalli.kawabe.entities.video.VideoListMutationDTO;
import com.thorgalli.kawabe.entities.video.VideoListQueryDTO;
import com.thorgalli.kawabe.repositories.VideoListRepository;
import com.thorgalli.kawabe.services.VideoListService;

@RestController
@RequestMapping("/video-lists")
public class VideoListController {
    private final VideoListService videoListService;
    private final VideoListRepository videoListRepository;

    @Autowired
    public VideoListController(VideoListService videoListService, VideoListRepository videoListRepository) {
        this.videoListService = videoListService;
        this.videoListRepository = videoListRepository;
    }

    @GetMapping()
    public ResponseEntity<List<VideoListQueryDTO>> getVideoLists() {
        List<VideoList> videoLists = videoListRepository.findAll();
        List<VideoListQueryDTO> videoListQueryDTOs = new ArrayList<>();
        for (VideoList videoList : videoLists) {
            VideoListQueryDTO videoListQueryDTO = videoListService.toDTO(videoList);
            videoListQueryDTOs.add(videoListQueryDTO);
        }
        return ResponseEntity.ok(videoListQueryDTOs);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @GetMapping("/{id}")
    public ResponseEntity<VideoListQueryDTO> getVideoListById(@PathVariable UUID id) {
        Optional<VideoList> videoListOptional = videoListRepository.findById(id);
        if (videoListOptional.isPresent()) {
            return ResponseEntity.ok(videoListService.toDTO(videoListOptional.get()));
        } else {
            return new ResponseEntity("VideoList not found with id: " + id, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<VideoListQueryDTO> createVideoList(@RequestBody VideoListMutationDTO createVideoListDTO) {
        VideoList newVideoList = videoListService.createVideoList(createVideoListDTO);
        return ResponseEntity.ok(videoListService.toDTO(newVideoList));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VideoListQueryDTO> updateVideoList(@PathVariable UUID id,
            @RequestBody VideoListMutationDTO updateVideoListDTO) {
        VideoList updatedVideoList = videoListService.updateVideoList(id, updateVideoListDTO);
        return ResponseEntity.ok(videoListService.toDTO(updatedVideoList));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteVideoList(@PathVariable UUID id) {
        if (!videoListRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        videoListRepository.deleteById(id);
        Map<String, String> response = new HashMap<>();
        response.put("message", "VideoList com ID " + id + " foi excluído com sucesso.");
        return ResponseEntity.ok(response);
    }
}
