package com.thorgalli.kawabe.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.thorgalli.kawabe.entities.photo.Photo;
import com.thorgalli.kawabe.entities.photo.PhotoDTO;
import com.thorgalli.kawabe.repositories.PhotoRepository;
import com.thorgalli.kawabe.services.PhotoService;

@RestController
@RequestMapping("/photos")
public class PhotoController {
  private final PhotoService photoService;
  private final PhotoRepository photoRepository;

  @Autowired
  public PhotoController(PhotoService photoService, PhotoRepository photoRepository) {
    this.photoService = photoService;
    this.photoRepository = photoRepository;
  }

  @GetMapping()
  public List<Photo> getPhotos() {
    return photoRepository.findAll();
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @GetMapping("/{id}")
  public ResponseEntity<Photo> getPhotoById(@PathVariable UUID id) {
    Optional<Photo> photoOptional = photoRepository.findById(id);
    if (photoOptional.isPresent()) {
      return ResponseEntity.ok(photoOptional.get());
    } else {
      return new ResponseEntity("Photo not found with id: " + id, HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping(consumes = { "multipart/form-data" })
  public ResponseEntity<Photo> createPhoto(
      @RequestPart("photo") PhotoDTO createPhotoDTO,
      @RequestPart(value = "file", required = false) MultipartFile file) {

    Photo newPhoto = photoService.createPhoto(createPhotoDTO, file);
    photoRepository.save(newPhoto);
    return ResponseEntity.ok(newPhoto);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deletePhoto(@PathVariable UUID id) {
    if (!photoRepository.existsById(id)) {
      return ResponseEntity.notFound().build();
    }

    photoRepository.deleteById(id);
    Map<String, String> response = new HashMap<>();
    response.put("message", "Foto com ID " + id + " foi excluída com sucesso.");
    return ResponseEntity.ok(response);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Photo> updatePhoto(@PathVariable UUID id, @RequestPart("photo") PhotoDTO updatePhotoDTO,
      @RequestPart(value = "file", required = false) MultipartFile file) {
    Photo existingPhoto = photoRepository.findById(id).orElseThrow(
      () -> new ResourceNotFoundException("Photo not found with id: " + id));

    Photo photo = photoService.updatePhoto(existingPhoto, updatePhotoDTO, file);
    photoRepository.save(photo);
    return ResponseEntity.ok(photo);
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
      super(message);
    }
  }
}
