package com.thorgalli.kawabe.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.thorgalli.kawabe.entities.course.CourseQueryDTO;
import com.thorgalli.kawabe.entities.user.User;
import com.thorgalli.kawabe.entities.user.UserPutDTO;
import com.thorgalli.kawabe.entities.user.UserQueryDTO;
import com.thorgalli.kawabe.repositories.UserRepository;
import com.thorgalli.kawabe.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
  private final UserRepository userRepository;
  private final UserService userService;

  @Autowired
  public UserController(UserRepository userRepository, UserService userService) {
    this.userRepository = userRepository;
    this.userService = userService;
  }

  @GetMapping()
  public List<UserQueryDTO> getUsers() {
      return userRepository.findAll().stream()
      .map(user -> new UserQueryDTO(
          user.getId(),
          user.getName(),
          user.getEmail(),
          user.getCourse() != null ? new CourseQueryDTO(
            user.getCourse().getId(),
            user.getCourse().getNamePortuguese(),
            user.getCourse().getNameJapanese(),
            user.getCourse().getImageURL(),
            user.getCourse().getDescription()
        ) : null,
          user.getRole()))
          .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  public ResponseEntity<User> getUserById(@PathVariable UUID id) {
    Optional<User> userOptional = userRepository.findById(id);
    if (userOptional.isPresent()) {
      return ResponseEntity.ok(userOptional.get());
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping()
  public ResponseEntity<User> createUser(@RequestBody User user) {
    User createdUser = userService.createUser(user);
    return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
  }

  @PutMapping("/{id}")
  public ResponseEntity<User> updateUser(@PathVariable UUID id, @RequestBody UserPutDTO userDetails) {
    User existingUser = userRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + id));

    User updatedUser = userService.updateUser(existingUser, userDetails);
    return ResponseEntity.ok(updatedUser);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteUser(@PathVariable UUID id) {
    if (!userRepository.existsById(id)) {
      return ResponseEntity.notFound().build();
    }

    userRepository.deleteById(id);
    Map<String, String> response = new HashMap<>();
    response.put("message", "User with ID " + id + " was successfully deleted.");
    return ResponseEntity.ok(response);
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
      super(message);
    }
  }
}
