package com.thorgalli.kawabe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thorgalli.kawabe.entities.user.AuthDTO;
import com.thorgalli.kawabe.entities.user.AuthResponse;
import com.thorgalli.kawabe.entities.user.RegisterDTO;
import com.thorgalli.kawabe.entities.user.User;
import com.thorgalli.kawabe.entities.user.UserRole;
import com.thorgalli.kawabe.infra.security.TokenService;
import com.thorgalli.kawabe.repositories.UserRepository;

@RestController
@RequestMapping("auth")
public class AuthController {
  private final AuthenticationManager authenticationManager;
  private final UserRepository userRepository;
  private final TokenService tokenService;

  @Autowired
  public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository,
      TokenService tokenService) {
    this.authenticationManager = authenticationManager;
    this.userRepository = userRepository;
    this.tokenService = tokenService;
  }

  @SuppressWarnings("rawtypes")
  @PostMapping("/login")
  public ResponseEntity login(@RequestBody AuthDTO authDTO) {
    var usernamePassword = new UsernamePasswordAuthenticationToken(authDTO.email(), authDTO.password());
    var auth = authenticationManager.authenticate(usernamePassword);
    var token = tokenService.generateToken((User) auth.getPrincipal());

    // I want to also return the user's role in the response
    UserRole role = ((User) auth.getPrincipal()).getRole();

    return ResponseEntity.ok(new AuthResponse(token, role));
  }

  @SuppressWarnings("rawtypes")
  @PostMapping("/register")
  public ResponseEntity register(@RequestBody RegisterDTO registerDTO) {
    if (this.userRepository.findByEmail(registerDTO.email()) != null)
      return ResponseEntity.badRequest().build();

    String encryptedPassString = new BCryptPasswordEncoder().encode(registerDTO.password());
    User newUser = new User(registerDTO.email(), encryptedPassString, registerDTO.name(), registerDTO.role());
    this.userRepository.save(newUser);
    return ResponseEntity.ok().build();
  }
}
