package com.thorgalli.kawabe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thorgalli.kawabe.entities.user.UserQueryDTO;
import com.thorgalli.kawabe.services.UserService;

@RestController
@RequestMapping("/me")
public class MeController {

    private final UserService userService;

    @Autowired
    public MeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<UserQueryDTO> getCurrentUser(Authentication authentication) {
        // Retrieve the current authenticated user's email from the token
        String userEmail = authentication.getName();
        
        // Check if the token is valid
        if (userEmail == null || userEmail.isEmpty()) {
            return ResponseEntity.status(401).build(); // Unauthorized
        }

        // Fetch user details using the email
        UserQueryDTO userDto = userService.getUserByEmail(userEmail);

        // Return the user details
        return ResponseEntity.ok(userDto);
    }
}
